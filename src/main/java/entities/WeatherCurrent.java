package entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalTime;

@Entity
@Getter
@Setter
public class WeatherCurrent {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int weatherId;
    private LocalDate dateReceivedInfo;
    private LocalDate dateForInfo;
    private long dateTimestamp;
    private LocalTime timeInfo;
    private Integer temp;
    private double humidityLevel;
    private double windSpeed;
    private double atmosphericPressure;
    private int weatherType;


    @ManyToOne
    @JoinColumn(name = "cityId", nullable = false)
    private City cityId;

}
