package entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class City {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int cityId;
    @Column(unique = true)
    private String cityName;
    private String countryName;
    private int defaultCity;
    @OneToMany(mappedBy = "cityId")
    private List<WeatherForecast> weathers;

    public City(String city_name) {
        this.cityName = city_name;
    }


}
