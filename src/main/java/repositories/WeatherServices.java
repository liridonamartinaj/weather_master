package repositories;

import apiActions.ApiCurrentWeatherResponse;
import apiActions.ApiForecastResponse;
import apiActions.ApiRequest;
import dbActions.DatabaseQueries;
import dbActions.StoreWeatherData;
import entities.City;
import entities.WeatherForecast;
import gui.*;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class WeatherServices {

    public static String default_city(String city_name) throws IOException {

        ApiRequest apiRequest = new ApiRequest();
        ApiCurrentWeatherResponse weatherData = apiRequest.getWeatherByCity(city_name);
        if (weatherData.getMessage() != null && weatherData.getMessage().equals("city not found")) {
            System.out.println("City not found");
        }
        StoreWeatherData.setDataForStorageCurrentWeather(city_name, weatherData);

        try {
            return " [" + city_name + " " + weatherData.getMainWeatherData().getTemp() + "°C "
                    + WeatherDateUtil.getWeatherSymbol(weatherData.getWeatherCode().get(0).getId()) + "]";

        } catch (Exception e) {
            return "";
        }
    }

    public static void weather_by_city(String city_name) throws IOException {
        ApiRequest apiRequest = new ApiRequest();
        ApiCurrentWeatherResponse weatherData = apiRequest.getWeatherByCity(city_name);

        if (weatherData.getMessage() != null && weatherData.getMessage().equals("city not found")) {
            System.out.println(Messages.CITY_NOT_FOUND.getMessage());

        } else {

            System.out.println("City: " + city_name + ", " + weatherData.getCityOtherInfo().getCountry());
            System.out.println("Temperature: " + weatherData.getMainWeatherData().getTemp() + "°C "
                    + WeatherDateUtil.getWeatherSymbol(weatherData.getWeatherCode().get(0).getId()));
            System.out.println("Humidity: " + weatherData.getMainWeatherData().getHumidity() + "%");
            System.out.println("Wind Speed: " + weatherData.getWindInfo().getSpeed() + " m/s");
            System.out.println("Atmospheric Pressure: " + weatherData.getMainWeatherData().getPressure());

            StoreWeatherData.setDataForStorageCurrentWeather(city_name, weatherData);
        }
    }

    public static void forecast_by_city_all(String city_name) throws IOException {
        ApiRequest apiRequest = new ApiRequest();
        ApiForecastResponse forecastData = apiRequest.getForecastByCity(city_name);

        if (forecastData.getMessage() != null && forecastData.getMessage().equals("city not found")) {
            System.out.println(Messages.CITY_NOT_FOUND.getMessage());

        } else try {
            for (ApiForecastResponse.ForecastData forecastDataAll : forecastData.getList()) {
                StoreWeatherData.setDataForStorageForecast(city_name, forecastDataAll);
            }
        } catch (Exception e) {
            System.out.println(Messages.SOMETHING_WRONG.getMessage());

        }
        List<WeatherForecast> weatherForCities = DatabaseQueries.getForecastTempAll(city_name);
        for (WeatherForecast weatherList : weatherForCities) {
            System.out.println(WeatherDateUtil.dateTimeFromTimestampTimezoned(weatherList.getDateTimestamp()) + " -> " + weatherList.getTemp() + "°C " + WeatherDateUtil.getWeatherSymbol(weatherList.getWeatherType()));

        }
    }

    public static void showPastWeather(String city_name, String date_string) throws IOException {
        City city = new DatabaseQueries().getCityByName(city_name);
        LocalDate date = null;
        try {
            date = LocalDate.parse(date_string);
        } catch (Exception e) {
            System.out.println(ColorsForText.red + "Date format is not correct" + ColorsForText.backToDefault);
            UserActions.userOptions();
        }


        try {
            int avgTemp = DatabaseQueries.getAverageTempFromDatabase(city, date, "Current").intValue();
            int minTemp = DatabaseQueries.getMinTempFromDatabase(city, date, "Current");
            int maxTemp = DatabaseQueries.getMaxTempFromDatabase(city, date, "Current");

            System.out.println(ColorsForText.purple + "On " + WeatherDateUtil.dateFormatted(date) + ", in " + city.getCityName() + " " + city.getCountryName() + ", there was an average temperature of "
                    + avgTemp + "°C. \nMinimum temperature registered was " + minTemp + "°C and maximum was " + maxTemp + "°C." + ColorsForText.backToDefault);

        } catch (Exception e) {
            System.out.println("No weather data found for " + city_name + " on " + date_string);
        }
    }

    public static void forecast_by_city(String city_name) {
        ApiRequest apiRequest = new ApiRequest();

        try {
            ApiForecastResponse forecastData = apiRequest.getForecastByCity(city_name);


            for (ApiForecastResponse.ForecastData forecastDataAll : forecastData.getList()) {
                StoreWeatherData.setDataForStorageForecast(city_name, forecastDataAll);
            }

            if (forecastData.getMessage() != null && forecastData.getMessage().equals("city not found")) {
                System.out.println(Messages.CITY_NOT_FOUND.getMessage());

            } else try {
                int totali = 108;

                System.out.println("#".repeat((totali - 34 - city_name.length()) / 2)
                        + " ".repeat(5) + "Weather Forecast for " + city_name + " ".repeat(5)
                        + "#".repeat((totali - 34 - city_name.length()) / 2)
                        + "#".repeat((totali - 34 - city_name.length()) % 2));
                System.out.print(" ");

                LocalDate dates;
                for (int i = 1; i < 5; i++) {
                    dates = LocalDate.now().plusDays(i);
                    System.out.print(" ".repeat(4)
                            + WeatherDateUtil.getWeatherSymbol(DatabaseQueries.getForecastFrequentWeatherType(city_name, dates))
                            + " " + WeatherDateUtil.dateFormatted(dates) + " "
                            + WeatherDateUtil.getWeatherSymbol(DatabaseQueries.getForecastFrequentWeatherType(city_name, dates))
                            + " ".repeat(4));
                }
                System.out.println();

                int i = 0;
                int j = 0;
                System.out.print(" ");

                City city = new DatabaseQueries().getCityByName(city_name);

                for (int g = 1; g < 5; g++) {
                    dates = LocalDate.now().plusDays(g);
                    System.out.print(" ".repeat(9 + i)
                            + DatabaseQueries.getMinTempFromDatabase(city, dates, "Forecast")
                            + "/" + DatabaseQueries.getMaxTempFromDatabase(city, dates, "Forecast")
                            + "°C");

                    i = 10;
                    j++;
                    if (j > 2) {
                        i = 9;
                    }
                }

                System.out.println();
                System.out.println("#".repeat(105));
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println(Messages.SOMETHING_WRONG.getMessage());
            }
        } catch (Exception e) {
            System.out.println(Messages.CITY_NOT_FOUND.getMessage());
        }
    }


    public static void test_forecast_by_city_all(String city_name) {

        ApiRequest apiRequest = new ApiRequest();
        try {
            ApiForecastResponse forecastData = apiRequest.getForecastByCity(city_name);
            for (ApiForecastResponse.ForecastData forecastDataAll : forecastData.getList()) {
                StoreWeatherData.setDataForStorageForecast(city_name, forecastDataAll);
            }

            if (forecastData.getMessage() != null && forecastData.getMessage().equals("city not found")) {
                System.out.println(Messages.CITY_NOT_FOUND.getMessage());

            } else try {
                City city = new DatabaseQueries().getCityByName(city_name);

                int totali = 108;

                System.out.println("#".repeat((totali - 34 - city_name.length()) / 2)
                        + " ".repeat(5) + "Weather Forecast for " + city_name + " ".repeat(5)
                        + "#".repeat((totali - 34 - city_name.length()) / 2)
                        + "#".repeat((totali - 34 - city_name.length()) % 2));

                LocalDate dates;
                System.out.print("            ");
                for (int i = 0; i < 5; i++) {
                    dates = LocalDate.now().plusDays(i);
                    System.out.print(" ".repeat(4)
                            + WeatherDateUtil.dateFormatted(dates) + " ".repeat(4));
                }

                List<String> stringList = List.of("00:00:00", "03:00:00", "06:00:00", "09:00:00", "12:00:00", "15:00:00", "18:00:00", "21:00:00");

                ZoneId defaultZone = ZoneId.systemDefault();

                for (String time_text : stringList) {
                    System.out.println();

                    LocalTime locale = LocalTime.now();

                    LocalTime localTime = LocalTime.parse(time_text);
                    ZonedDateTime utcTime = ZonedDateTime.of(LocalDate.now(), localTime, ZoneId.of("UTC"));
                    ZonedDateTime localTimed = utcTime.withZoneSameInstant(defaultZone);

                    DateTimeFormatter formater = DateTimeFormatter.ofPattern("HH:mm");
                    String formattedTime = formater.format(localTimed.toLocalTime());

                    System.out.print(formattedTime + ":");
                    for (int g = 0; g < 5; g++) {
                        dates = LocalDate.now().plusDays(g);
                        if (locale.isAfter(localTimed.toLocalTime()) && dates.isEqual(LocalDate.now())) {
                            System.out.print("                   ");
                        } else {

                            System.out.print(" ".repeat(12)
                                    + DatabaseQueries.getForecastTempAllForHour(city, dates, localTime) + "°C " + WeatherDateUtil.getWeatherSymbol(DatabaseQueries.getForecastWeatherTypeForHour(city, dates, localTime)));


                        }
                    }

                }
                System.out.println();
                System.out.println("#".repeat(105));
            } catch (Exception ex) {
                throw new RuntimeException(ex);


            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(Messages.SOMETHING_WRONG.getMessage());
        }

    }
}


