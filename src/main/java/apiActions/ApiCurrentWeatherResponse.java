package apiActions;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class ApiCurrentWeatherResponse {


    @JsonProperty("main")
    private mainWeatherData mainWeatherData;

    @JsonProperty("wind")
    private WindInfo windInfo;

    @JsonProperty("weather")
    private List<WeatherCode> weatherCode;

    @JsonProperty("sys")
    private cityOtherInfo cityOtherInfo;
    private String message;
    private long dt;

    @Getter
    @Setter
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class mainWeatherData {
        private double temp;
        private int pressure;
        private int humidity;

        public int getTemp() {
            return (int) Math.round(temp);
        }

    }

    @Getter
    @Setter
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class WindInfo {
        private double speed;
    }

    @Getter
    @Setter
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class WeatherCode {
        private int id;
    }

    @Getter
    @Setter
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class cityOtherInfo {
        private String country;
    }

}