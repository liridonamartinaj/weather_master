package apiActions;

import com.fasterxml.jackson.databind.ObjectMapper;
import gui.Messages;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;

public class ApiRequest {
    private final OkHttpClient okHttpClient = new OkHttpClient();
    private final ObjectMapper objectMapper = new ObjectMapper();


    private String requestApiResponse(String apiUrl) {
        Request request = new Request.Builder().url(apiUrl).build();
        try (Response response = okHttpClient.newCall(request).execute()) {
            return response.body().string();
        } catch (Exception e) {
            System.out.println(Messages.SOMETHING_WRONG.getMessage());
            return null;
        }
    }
    public ApiCurrentWeatherResponse getWeatherByCity(String city_name) throws IOException {
        String apiUrl = "https://api.openweathermap.org/data/2.5/weather?units=metric&appid=30d43c790a5ff92c061bdaf9c866a004&q=" + city_name;
        String responseBody = requestApiResponse(apiUrl);

        if (responseBody == null) {
            return null;
        } else {
            return objectMapper.readValue(responseBody, ApiCurrentWeatherResponse.class);
        }
    }

    public ApiForecastResponse getForecastByCity(String city_name) throws IOException {
        String apiUrl = "https://api.openweathermap.org/data/2.5/forecast?units=metric&appid=30d43c790a5ff92c061bdaf9c866a004&q=" + city_name;
        String responseBody = requestApiResponse(apiUrl);

        if (responseBody == null) {
            return null;
        } else {
            return objectMapper.readValue(responseBody, ApiForecastResponse.class);
        }
    }
}
