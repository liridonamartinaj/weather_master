package apiActions;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class ApiForecastResponse {



    private List<ForecastData> list;

    private String message;

    @Getter
    @Setter
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class ForecastData {
        private String dt_txt;
        private mainWeatherData main;
        private WindInfo wind;
        @JsonProperty("weather")
        private List<WeatherType> weatherType;

        @JsonProperty("sys")
        private ApiCurrentWeatherResponse.cityOtherInfo cityOtherInfo;
        private String message;
        private long dt;

    }

    @Getter
    @Setter
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class mainWeatherData {
        private double temp;
        private double pressure;
        private int humidity;


    public int getTemp() {
        return (int)Math.round(temp);
    }

}

    @Getter
    @Setter
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class WindInfo {
        private double speed;
    }



    @Getter
    @Setter
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class WeatherType {
        private int id;
    }

    @Getter
    @Setter
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class cityOtherInfo {
        private String country;
    }

}

