package gui;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Messages {
    THURJE("\u26A1 \u2600\ufe0f \u2614 "),
    WELCOME("\u2600\ufe0f" + ColorsForText.yellow + "  WELCOME to WeatherMASTER  " + ColorsForText.backToDefault + "\u2600\ufe0f"),
    OPTION_TO_SHOW_CURRENT_WEATHER("Show current weather for a city"),
    OPTION_TO_SHOW_FORECAST("Show forecast for a city"),
    OPTION_TO_SHOW_FORECAST_ALL("Show forecast every 3 hour for a city"),
    OPTION_TO_EXIT_APP(ColorsForText.red + "Exit application" + ColorsForText.backToDefault),
    DATE_INCORRECT_FORMAT(ColorsForText.red + "Date format is not correct" + ColorsForText.backToDefault),
    WRONG_OPTION_CHOSEN(ColorsForText.purple + "Option does not exist" + ColorsForText.backToDefault),
    TELL_TO_CHOOSE_OPTIONS(ColorsForText.green + "Choose the desired action: " + ColorsForText.backToDefault),
    TELL_TO_CHOOSE_CITY("Enter the city: "),
    TELL_TO_CHOOSE_DATE("Enter date (yyyy-mm-dd): "),
    CITY_NOT_FOUND(ColorsForText.red + "City not found" + ColorsForText.backToDefault),
    PERFORM_OR_NOT_OTHER_ACTIONS(ColorsForText.blue + "Do you want to do something else? \n 1.Yes(default)       2.No" + ColorsForText.backToDefault),
    EXITING_APPLICATION(ColorsForText.purple + "Thank YOU for using WeatherMASTER. See YOU soon!" + ColorsForText.backToDefault),
    SOMETHING_WRONG(ColorsForText.red + "Something went wrong" + ColorsForText.backToDefault);

    private final String message;

}
