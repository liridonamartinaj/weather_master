package gui;

import repositories.WeatherServices;

import java.io.IOException;
import java.util.Scanner;

public class UserActions {

    private static final Scanner scanner = new Scanner(System.in);

    public static void userOptions() throws IOException {
        while (true) {
            System.out.println();
            System.out.println(Messages.TELL_TO_CHOOSE_OPTIONS.getMessage());
            System.out.println("1. " + Messages.OPTION_TO_SHOW_CURRENT_WEATHER.getMessage());
            System.out.println("2. " + Messages.OPTION_TO_SHOW_FORECAST.getMessage());
            System.out.println("3. " + Messages.OPTION_TO_SHOW_FORECAST_ALL.getMessage());
            System.out.println(ColorsForText.red + "5. " + Messages.OPTION_TO_EXIT_APP.getMessage());

            String userChosenOption = scanner.nextLine();

            switch (userChosenOption) {
                case "1" -> currentWeatherOption();
                case "2" -> forecastOption();
                case "3" -> forecastAllOption();
                case "5" -> {
                    exitApplication();
                    return;
                }
                default -> System.out.println(Messages.WRONG_OPTION_CHOSEN.getMessage());
            }

            System.out.println(Messages.PERFORM_OR_NOT_OTHER_ACTIONS.getMessage());
            String performOrNotOtherActions = scanner.nextLine();
            if (performOrNotOtherActions.equals("2")) {
                exitApplication();
                return;
            }
        }
    }

    private static void currentWeatherOption() throws IOException {
        System.out.println(Messages.TELL_TO_CHOOSE_CITY.getMessage());
        String city = scanner.nextLine();
        WeatherServices.weather_by_city(city);
    }

    private static void forecastOption(){
        System.out.println(Messages.TELL_TO_CHOOSE_CITY.getMessage());
        String city = scanner.nextLine();
        WeatherServices.forecast_by_city(city);
    }

    private static void forecastAllOption(){
        System.out.println(Messages.TELL_TO_CHOOSE_CITY.getMessage());
        String city = scanner.nextLine();
        WeatherServices.test_forecast_by_city_all(city);
    }



    private static void exitApplication() {
        System.out.println(Messages.EXITING_APPLICATION.getMessage());
        scanner.close();
        System.exit(0);
    }

}
