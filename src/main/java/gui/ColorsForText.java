package gui;

public class ColorsForText {
    public static final String yellow = "\u001B[93m";
    public static final String blue = "\u001B[96m";
    public static final String red = "\u001B[91m";
    public static final String green = "\u001B[92m";
    public static final String purple = "\u001B[95m";
    public static final String backToDefault = "\u001B[0m";
}
