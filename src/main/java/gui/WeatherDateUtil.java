package gui;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

public class WeatherDateUtil {
    public static String getWeatherSymbol(int weatherCode) {
        return switch (weatherCode) {
            case 200, 201, 202, 230, 231, 232 -> "\u26C8\ufe0f";
            case 210, 211, 212, 221 -> "\u26A1\ufe0f";
            case 300, 301, 302, 310, 311, 312, 313, 314, 321, 500, 501, 502, 503, 504 -> "\u2614\ufe0f";
            case 511, 600, 601, 602, 611, 612, 615, 616, 620, 622, 621 -> "\u2744\ufe0f";
            case 520, 521, 522, 531 -> "\u2614";
            case 701, 711, 721, 731, 741 -> "\uD83C\uDF2B";
            case 751, 761, 762, 771 -> "\uD83D\uDCA8";
            case 781 -> "\uD83C\uDF2A";
            case 800 -> "\u2600\ufe0f";
            case 801, 802 -> "\uD83C\uDF24\ufe0f";
            case 803, 804 -> "\u2601\ufe0f";
            default -> null;
        };


    }

    public static String dateTimeFromTimestampTimezoned(long timestamp) {
        ZonedDateTime dateTime = Instant.ofEpochSecond(timestamp)
                .atZone(ZoneId.systemDefault());
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MMM HH:mm", Locale.ENGLISH);

        return dateTime.format(formatter);

    }
    public static String dateFromTimestampTimezoned(long timestamp) {
        ZonedDateTime dateTime = Instant.ofEpochSecond(timestamp)
                .atZone(ZoneId.systemDefault());
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd MMM yyyy", Locale.ENGLISH);

        return dateTime.format(formatter);

    }

    public static String dateFormatted (LocalDate date) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd MMM yyyy", Locale.ENGLISH);
        return date.format(formatter);


    }

    public static String dateFromAPIFormated(String dt_txt) {
        String[] parts = dt_txt.split(" ");
        String[] parts2 = parts[0].split("-");
        return parts2[2] + "-" + parts2[1] + "-" + parts2[0];
    }

    public static LocalDate dateFromAPIParse(String dt_txt) {
        String[] parts = dt_txt.split(" ");
        return LocalDate.parse(parts[0]);
    }

    public static LocalTime timeFromAPIParse(String dt_txt) {
        String[] parts = dt_txt.split(" ");
        return LocalTime.parse(parts[1]);
    }


}
