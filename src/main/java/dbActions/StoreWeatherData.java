package dbActions;

import apiActions.ApiCurrentWeatherResponse;
import apiActions.ApiForecastResponse;
import entities.City;
import entities.WeatherCurrent;
import entities.WeatherForecast;
import gui.WeatherDateUtil;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

public class StoreWeatherData {

    public static void setDataForStorageForecast(String city_name, ApiForecastResponse.ForecastData forecastDataAll) {
        DatabaseQueries checkForCity = new DatabaseQueries();
        City city = checkForCity.getCityByName(city_name);

        if (city == null) {
            city = new City(city_name);
            city.setCountryName(forecastDataAll.getCityOtherInfo().getCountry());
            HibernateUtil.storeWeatherData(city);
        }

        long dateTimestamp = forecastDataAll.getDt();

        WeatherForecast existingForecast = DatabaseQueries.getExistingWeatherForecast(city, dateTimestamp);

        if (existingForecast != null) {
            existingForecast.setDateReceivedInfo(LocalDate.now());
            existingForecast.setTemp(forecastDataAll.getMain().getTemp());
            existingForecast.setAtmosphericPressure(forecastDataAll.getMain().getPressure());
            existingForecast.setHumidityLevel(forecastDataAll.getMain().getHumidity());
            existingForecast.setWindSpeed(forecastDataAll.getWind().getSpeed());
            existingForecast.setWeatherType(forecastDataAll.getWeatherType().get(0).getId());
            existingForecast.setCityId(city);
            existingForecast.setDateTimestamp(dateTimestamp);
            existingForecast.setDateForInfo(WeatherDateUtil.dateFromAPIParse(forecastDataAll.getDt_txt()));
            existingForecast.setTimeInfo(WeatherDateUtil.timeFromAPIParse(forecastDataAll.getDt_txt()));

            HibernateUtil.storeWeatherData(existingForecast);
        } else {
            WeatherForecast weather = new WeatherForecast();
            weather.setDateReceivedInfo(LocalDate.now());
            weather.setTemp(forecastDataAll.getMain().getTemp());
            weather.setAtmosphericPressure(forecastDataAll.getMain().getPressure());
            weather.setHumidityLevel(forecastDataAll.getMain().getHumidity());
            weather.setWindSpeed(forecastDataAll.getWind().getSpeed());
            weather.setWeatherType(forecastDataAll.getWeatherType().get(0).getId());
            weather.setCityId(city);
            weather.setDateTimestamp(dateTimestamp);
            weather.setDateForInfo(WeatherDateUtil.dateFromAPIParse(forecastDataAll.getDt_txt()));
            weather.setTimeInfo(WeatherDateUtil.timeFromAPIParse(forecastDataAll.getDt_txt()));

            HibernateUtil.storeWeatherData(weather);
            }
        }



    public static void setDataForStorageCurrentWeather(String city_name, ApiCurrentWeatherResponse weatherData) {
        DatabaseQueries checkForCity = new DatabaseQueries();
        City city = checkForCity.getCityByName(city_name);

        if (city == null) {
            city = new City(city_name);
            city.setCountryName(weatherData.getCityOtherInfo().getCountry());
        }
        LocalDateTime localDateTime = LocalDateTime.now();
        long dateTimestamp = localDateTime.toEpochSecond(ZoneOffset.UTC);

        WeatherCurrent weather = new WeatherCurrent();
        weather.setDateReceivedInfo(LocalDate.now());
        weather.setTemp(weatherData.getMainWeatherData().getTemp());
        weather.setAtmosphericPressure(weatherData.getMainWeatherData().getPressure());
        weather.setHumidityLevel(weatherData.getMainWeatherData().getHumidity());
        weather.setWindSpeed(weatherData.getWindInfo().getSpeed());
        weather.setDateForInfo(LocalDate.now());
        weather.setDateTimestamp(dateTimestamp);
        weather.setCityId(city);
        weather.setWeatherType(weatherData.getWeatherCode().get(0).getId());

        if (city.getCityId() == 0) {
            HibernateUtil.storeWeatherData(city, weather);
        } else {
            HibernateUtil.storeWeatherData(city, weather);
        }


    }


}
