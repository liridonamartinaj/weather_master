package dbActions;

import entities.City;
import entities.WeatherCurrent;
import entities.WeatherForecast;
import gui.Messages;
import lombok.Getter;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

@Getter
public class HibernateUtil {

    static final SessionFactory sessionFactory = new Configuration()
            .configure("hibernate.cfg.xml")
            .buildSessionFactory();

    public static void storeWeatherData(WeatherForecast weather) {
        try (Session session = HibernateUtil.sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();

            if (weather.getWeatherId() != 0) {
                session.merge(weather);
            } else {
                session.persist(weather);
            }

            transaction.commit();
        } catch (Exception e) {
            System.out.println(Messages.SOMETHING_WRONG.getMessage());
        }
    }

    public static void storeWeatherData(City city) {
        Transaction transaction = null;

        try (Session session = HibernateUtil.sessionFactory.openSession()) {
            transaction = session.beginTransaction();

            session.persist(city);

            transaction.commit();
        } catch (Exception e) {
            if (transaction != null && transaction.isActive()) {
                transaction.rollback();
            }
        }
    }

    public static void storeWeatherData(City city, WeatherCurrent weather) {
        try (Session session = HibernateUtil.sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();

            if (city.getCityId() == 0) {
                session.persist(city);
            }

            if (weather.getWeatherId() != 0) {
                session.merge(weather);
            } else {
                session.persist(weather);
            }

            transaction.commit();
        } catch (Exception e) {
            System.out.println(Messages.SOMETHING_WRONG.getMessage());
        }
    }


}
