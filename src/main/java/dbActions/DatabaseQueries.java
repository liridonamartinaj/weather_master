package dbActions;

import entities.City;
import entities.WeatherForecast;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

public class DatabaseQueries {
    public City getCityByName(String city_name) {

        try (Session session = HibernateUtil.sessionFactory.openSession()) {
            return session.createQuery("FROM City WHERE cityName = :name", City.class)
                    .setParameter("name", city_name).uniqueResult();
        }
    }
    public static Double getAverageTempFromDatabase(City city, LocalDate date, String writeCurrentOrForecast) {
        if (writeCurrentOrForecast.equals("Current")) {
            try (Session session = HibernateUtil.sessionFactory.openSession()) {
                return session.createQuery("SELECT Avg(temp) from WeatherCurrent WHERE " +
                                "cityId = :city AND dateForInfo = :date", Double.class)
                        .setParameter("city", city).setParameter("date", date).getSingleResult();

            }
        } else {
            try (Session session = HibernateUtil.sessionFactory.openSession()) {
                return session.createQuery("SELECT Avg(temp) from WeatherForecast " +
                                "WHERE cityId = :city AND dateForInfo = :date", Double.class)
                        .setParameter("city", city).setParameter("date", date).getSingleResult();

            }
        }
    }
    public static Integer getMinTempFromDatabase(City city, LocalDate date, String writeCurrentOrForecast) {
        try (Session session = HibernateUtil.sessionFactory.openSession()) {

            if (writeCurrentOrForecast.equals("Current")) {

                return session.createQuery("SELECT Min(temp) FROM WeatherCurrent WHERE cityId = :city AND dateForInfo = :date", Integer.class)
                        .setParameter("city", city).setParameter("date", date).getSingleResult();
            } else {
                return session.createQuery("SELECT Min(temp) FROM WeatherForecast WHERE cityId = :city AND dateForInfo = :date", Integer.class)
                        .setParameter("city", city).setParameter("date", date).getSingleResult();
            }

        }

    }
    public static Integer getMaxTempFromDatabase(City city, LocalDate date, String writeCurrentOrForecast) {
        try (Session session = HibernateUtil.sessionFactory.openSession()) {
            if (writeCurrentOrForecast.equals("Current")) {
                return session.createQuery("SELECT Max(temp) FROM WeatherCurrent " +
                                "WHERE cityId = :city AND dateForInfo = :date", Integer.class)
                        .setParameter("city", city).setParameter("date", date).getSingleResult();
            } else {
                return session.createQuery("SELECT Max(temp) FROM WeatherForecast " +
                                "WHERE cityId = :city AND dateForInfo = :date", Integer.class)
                        .setParameter("city", city).setParameter("date", date).getSingleResult();
            }

        }
    }
    public static List<WeatherForecast> getForecastTempAll(String city) {
        try (Session session = HibernateUtil.sessionFactory.openSession()) {
            City city1 = new DatabaseQueries().getCityByName(city);

            Query<WeatherForecast> query = session.createQuery("FROM WeatherForecast weather" +
                            " WHERE weather.cityId = :city AND weather.dateTimestamp > :date", WeatherForecast.class)
                    .setParameter("city", city1).setParameter("date", Instant.now().getEpochSecond());

            return query.list();
        }
    }

    public static Integer getForecastTempAllForHour(City city, LocalDate date, LocalTime time) {
        try (Session session = HibernateUtil.sessionFactory.openSession()) {

            return session.createQuery("Select weather.temp FROM WeatherForecast weather" +
                            " WHERE weather.cityId = :city AND weather.dateForInfo = :date and weather.timeInfo = :timeInfo", Integer.class)
                    .setParameter("city", city).setParameter("date", date).setParameter("timeInfo", time).getSingleResult();

        }
    }

    public static Integer getForecastWeatherTypeForHour(City city, LocalDate date, LocalTime time) {
        try (Session session = HibernateUtil.sessionFactory.openSession()) {

            return session.createQuery("Select weather.weatherType FROM WeatherForecast weather" +
                            " WHERE weather.cityId = :city AND weather.dateForInfo = :date and weather.timeInfo = :timeInfo", Integer.class)
                    .setParameter("city", city).setParameter("date", date).setParameter("timeInfo", time).getSingleResult();
        }
    }
    public static Integer getForecastFrequentWeatherType(String city, LocalDate date) {
        try (Session session = HibernateUtil.sessionFactory.openSession()) {
            City city1 = new DatabaseQueries().getCityByName(city);

            return session.createQuery("select weatherType " +
                            "from WeatherForecast where cityId = :city and dateForInfo = :date " +
                            "group by weatherType order by count(weatherType) desc limit 1", Integer.class)
                    .setParameter("city", city1).setParameter("date", date).getSingleResult();

        }
    }
    public static WeatherForecast getExistingWeatherForecast(City city, long timestamp) {
        try (Session session = HibernateUtil.sessionFactory.openSession()) {
            return session.createQuery("FROM WeatherForecast WHERE cityId = :city AND dateTimestamp = :timestamp", WeatherForecast.class)
                    .setParameter("city", city)
                    .setParameter("timestamp", timestamp)
                    .uniqueResult();
        }
    }
}